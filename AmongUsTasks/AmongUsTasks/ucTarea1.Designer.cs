﻿
namespace AmongUsTasks
{
    partial class ucTarea1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTítulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLedVerde = new System.Windows.Forms.Button();
            this.btnLedRojo = new System.Windows.Forms.Button();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.PanelTanque = new System.Windows.Forms.Panel();
            this.panelGasolina = new System.Windows.Forms.Panel();
            this.tmrLlenar = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PanelTanque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblTítulo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(600, 400);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTítulo
            // 
            this.lblTítulo.AutoSize = true;
            this.lblTítulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTítulo.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTítulo.Location = new System.Drawing.Point(3, 0);
            this.lblTítulo.Name = "lblTítulo";
            this.lblTítulo.Size = new System.Drawing.Size(594, 92);
            this.lblTítulo.TabIndex = 0;
            this.lblTítulo.Text = "TAREA 1";
            this.lblTítulo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLedVerde);
            this.panel1.Controls.Add(this.btnLedRojo);
            this.panel1.Controls.Add(this.btnLlenar);
            this.panel1.Controls.Add(this.PanelTanque);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(594, 302);
            this.panel1.TabIndex = 1;
            // 
            // btnLedVerde
            // 
            this.btnLedVerde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnLedVerde.Enabled = false;
            this.btnLedVerde.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLedVerde.FlatAppearance.BorderSize = 3;
            this.btnLedVerde.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLedVerde.Location = new System.Drawing.Point(436, 209);
            this.btnLedVerde.Name = "btnLedVerde";
            this.btnLedVerde.Size = new System.Drawing.Size(16, 17);
            this.btnLedVerde.TabIndex = 4;
            this.btnLedVerde.UseVisualStyleBackColor = false;
            // 
            // btnLedRojo
            // 
            this.btnLedRojo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnLedRojo.Enabled = false;
            this.btnLedRojo.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLedRojo.FlatAppearance.BorderSize = 3;
            this.btnLedRojo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLedRojo.Location = new System.Drawing.Point(404, 209);
            this.btnLedRojo.Name = "btnLedRojo";
            this.btnLedRojo.Size = new System.Drawing.Size(16, 17);
            this.btnLedRojo.TabIndex = 4;
            this.btnLedRojo.UseVisualStyleBackColor = false;
            // 
            // btnLlenar
            // 
            this.btnLlenar.BackColor = System.Drawing.Color.LightGray;
            this.btnLlenar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLlenar.FlatAppearance.BorderSize = 2;
            this.btnLlenar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLlenar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLlenar.Location = new System.Drawing.Point(401, 239);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(57, 47);
            this.btnLlenar.TabIndex = 2;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = false;
            this.btnLlenar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnLlenar_MouseDown);
            this.btnLlenar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnLlenar_MouseUp);
            // 
            // PanelTanque
            // 
            this.PanelTanque.BackColor = System.Drawing.Color.Black;
            this.PanelTanque.Controls.Add(this.panelGasolina);
            this.PanelTanque.Location = new System.Drawing.Point(172, 26);
            this.PanelTanque.Name = "PanelTanque";
            this.PanelTanque.Size = new System.Drawing.Size(180, 208);
            this.PanelTanque.TabIndex = 1;
            // 
            // panelGasolina
            // 
            this.panelGasolina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(156)))), ((int)(((byte)(12)))));
            this.panelGasolina.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGasolina.Location = new System.Drawing.Point(0, 93);
            this.panelGasolina.Name = "panelGasolina";
            this.panelGasolina.Size = new System.Drawing.Size(180, 115);
            this.panelGasolina.TabIndex = 0;
            // 
            // tmrLlenar
            // 
            this.tmrLlenar.Tick += new System.EventHandler(this.tmrLlenar_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AmongUsTasks.Properties.Resources.Fuel_Engines;
            this.pictureBox1.Location = new System.Drawing.Point(119, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(388, 296);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ucTarea1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "ucTarea1";
            this.Size = new System.Drawing.Size(600, 400);
            this.Load += new System.EventHandler(this.ucTarea1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.PanelTanque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTítulo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel PanelTanque;
        private System.Windows.Forms.Panel panelGasolina;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Timer tmrLlenar;
        private System.Windows.Forms.Button btnLedVerde;
        private System.Windows.Forms.Button btnLedRojo;
    }
}
