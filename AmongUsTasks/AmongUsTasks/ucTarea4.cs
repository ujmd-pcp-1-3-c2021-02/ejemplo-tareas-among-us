﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea4 : UserControl
    {
        public ucTarea4()
        {
            InitializeComponent();
        }

        private void ucTarea4_Load(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int[] DigitosAleatorios = new int[5];

            //lblID.Text = "";
            //for (int i = 0; i < DigitosAleatorios.Length; i++)
            //{
            //    DigitosAleatorios[i] = rnd.Next(0, 10);
            //    lblID.Text = lblID.Text + DigitosAleatorios[i];
            //}

            int NumAleatorio = rnd.Next(0, 100000);
            lblID.Text = NumAleatorio.ToString("00000");
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            ucTarea4_Load(sender, e);
        }

        private void panelTarjetaGuardada_Click(object sender, EventArgs e)
        {
            panelTarjeta.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btn = (Button) sender;

            if (lblDigitos.Text.Length < 5)
            {
                lblDigitos.Text = lblDigitos.Text + btn.Text;
                
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (lblDigitos.Text == lblID.Text)
            {
                lblTarea.Text = "TAREA 4 COMPLETADA";
                lblTarea.ForeColor = Color.Lime;
                btnClear.Enabled = false;
                btnOK.Enabled = false;

                frmPrincipal.TareasCompletadas[3] = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lblDigitos.Text = "";
        }
    }
}
