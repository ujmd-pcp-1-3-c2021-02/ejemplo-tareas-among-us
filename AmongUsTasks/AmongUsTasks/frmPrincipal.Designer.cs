﻿
namespace AmongUsTasks
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.panelHome = new System.Windows.Forms.Panel();
            this.ucTarea61 = new AmongUsTasks.ucTarea6();
            this.ucTarea51 = new AmongUsTasks.ucTarea5();
            this.ucTarea41 = new AmongUsTasks.ucTarea4();
            this.ucTarea31 = new AmongUsTasks.ucTarea3();
            this.ucTarea21 = new AmongUsTasks.ucTarea2();
            this.ucTarea11 = new AmongUsTasks.ucTarea1();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rbtnHome = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panelPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelPrincipal, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radioButton6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radioButton5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radioButton4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radioButton3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radioButton2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radioButton1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rbtnHome, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 491);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPrincipal.Controls.Add(this.panelHome);
            this.panelPrincipal.Controls.Add(this.ucTarea61);
            this.panelPrincipal.Controls.Add(this.ucTarea51);
            this.panelPrincipal.Controls.Add(this.ucTarea41);
            this.panelPrincipal.Controls.Add(this.ucTarea31);
            this.panelPrincipal.Controls.Add(this.ucTarea21);
            this.panelPrincipal.Controls.Add(this.ucTarea11);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(155, 3);
            this.panelPrincipal.Name = "panelPrincipal";
            this.tableLayoutPanel1.SetRowSpan(this.panelPrincipal, 9);
            this.panelPrincipal.Size = new System.Drawing.Size(642, 485);
            this.panelPrincipal.TabIndex = 0;
            // 
            // panelHome
            // 
            this.panelHome.BackgroundImage = global::AmongUsTasks.Properties.Resources.Among_Us_menu;
            this.panelHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHome.Location = new System.Drawing.Point(0, 0);
            this.panelHome.Name = "panelHome";
            this.panelHome.Size = new System.Drawing.Size(642, 485);
            this.panelHome.TabIndex = 0;
            // 
            // ucTarea61
            // 
            this.ucTarea61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea61.Location = new System.Drawing.Point(0, 0);
            this.ucTarea61.Name = "ucTarea61";
            this.ucTarea61.Size = new System.Drawing.Size(642, 485);
            this.ucTarea61.TabIndex = 6;
            this.ucTarea61.Visible = false;
            // 
            // ucTarea51
            // 
            this.ucTarea51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea51.Location = new System.Drawing.Point(0, 0);
            this.ucTarea51.Name = "ucTarea51";
            this.ucTarea51.Size = new System.Drawing.Size(642, 485);
            this.ucTarea51.TabIndex = 5;
            this.ucTarea51.Visible = false;
            // 
            // ucTarea41
            // 
            this.ucTarea41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea41.Location = new System.Drawing.Point(0, 0);
            this.ucTarea41.Name = "ucTarea41";
            this.ucTarea41.Size = new System.Drawing.Size(642, 485);
            this.ucTarea41.TabIndex = 4;
            this.ucTarea41.Visible = false;
            // 
            // ucTarea31
            // 
            this.ucTarea31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea31.Location = new System.Drawing.Point(0, 0);
            this.ucTarea31.Name = "ucTarea31";
            this.ucTarea31.Size = new System.Drawing.Size(642, 485);
            this.ucTarea31.TabIndex = 3;
            this.ucTarea31.Visible = false;
            // 
            // ucTarea21
            // 
            this.ucTarea21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea21.ForeColor = System.Drawing.Color.Black;
            this.ucTarea21.Location = new System.Drawing.Point(0, 0);
            this.ucTarea21.Name = "ucTarea21";
            this.ucTarea21.Size = new System.Drawing.Size(642, 485);
            this.ucTarea21.TabIndex = 2;
            this.ucTarea21.Visible = false;
            // 
            // ucTarea11
            // 
            this.ucTarea11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea11.ForeColor = System.Drawing.Color.Black;
            this.ucTarea11.Location = new System.Drawing.Point(0, 0);
            this.ucTarea11.Name = "ucTarea11";
            this.ucTarea11.Size = new System.Drawing.Size(642, 485);
            this.ucTarea11.TabIndex = 1;
            this.ucTarea11.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::AmongUsTasks.Properties.Resources.among_us_2073273_stb7;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(146, 146);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // radioButton6
            // 
            this.radioButton6.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton6.FlatAppearance.BorderSize = 3;
            this.radioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(3, 425);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(146, 39);
            this.radioButton6.TabIndex = 2;
            this.radioButton6.Text = "Tarea 6";
            this.radioButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton5.FlatAppearance.BorderSize = 3;
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(3, 380);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(146, 39);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.Text = "Tarea 5";
            this.radioButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton4.FlatAppearance.BorderSize = 3;
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(3, 335);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(146, 39);
            this.radioButton4.TabIndex = 2;
            this.radioButton4.Text = "Tarea 4";
            this.radioButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton3.FlatAppearance.BorderSize = 3;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.Location = new System.Drawing.Point(3, 290);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(146, 39);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "Tarea 3";
            this.radioButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton2.FlatAppearance.BorderSize = 3;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(3, 245);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(146, 39);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Tarea 2";
            this.radioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton1.FlatAppearance.BorderSize = 3;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(3, 200);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(146, 39);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "Tarea 1";
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbtnHome
            // 
            this.rbtnHome.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnHome.Checked = true;
            this.rbtnHome.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnHome.FlatAppearance.BorderSize = 3;
            this.rbtnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnHome.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHome.Location = new System.Drawing.Point(3, 155);
            this.rbtnHome.Name = "rbtnHome";
            this.rbtnHome.Size = new System.Drawing.Size(146, 39);
            this.rbtnHome.TabIndex = 2;
            this.rbtnHome.TabStop = true;
            this.rbtnHome.Text = "Inicio";
            this.rbtnHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnHome.UseVisualStyleBackColor = true;
            this.rbtnHome.CheckedChanged += new System.EventHandler(this.rbtnHome_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 491);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrincipal";
            this.Text = "Réplica de tareas de Among Us";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton rbtnHome;
        private System.Windows.Forms.Panel panelHome;
        private ucTarea6 ucTarea61;
        private ucTarea5 ucTarea51;
        private ucTarea4 ucTarea41;
        private ucTarea3 ucTarea31;
        private ucTarea2 ucTarea21;
        private ucTarea1 ucTarea11;
        private System.Windows.Forms.Timer timer1;
    }
}

