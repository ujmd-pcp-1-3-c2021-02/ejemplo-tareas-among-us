﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea6 : UserControl
    {
        public ucTarea6()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox) sender;
            if (chk.Checked)
            {
                chk.FlatAppearance.BorderColor = Color.Red;
            }
            else
            {
                chk.FlatAppearance.BorderColor = Color.White;

                if (!checkBox1.Checked && !checkBox2.Checked && 
                    !checkBox3.Checked && !checkBox4.Checked && 
                    !checkBox5.Checked && !checkBox6.Checked && 
                    !checkBox7.Checked)
                {
                    label1.Text = "TAREA 6 FINALIZADA";
                    label1.ForeColor = Color.Lime;
                    checkBox1.Enabled = false;
                    checkBox2.Enabled = false;
                    checkBox3.Enabled = false;
                    checkBox4.Enabled = false;
                    checkBox5.Enabled = false;
                    checkBox6.Enabled = false;
                    checkBox7.Enabled = false;

                    frmPrincipal.TareasCompletadas[5] = true;
                }
            }


        }

        private void ucTarea6_Load(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int[] NumAleatorios = new int[7];

            for (int i = 0; i < NumAleatorios.Length; i++)
            {
                NumAleatorios[i] = rnd.Next(0, 2);
            }

            checkBox1.Checked = Convert.ToBoolean(NumAleatorios[0]);
            checkBox2.Checked = Convert.ToBoolean(NumAleatorios[1]);
            checkBox3.Checked = Convert.ToBoolean(NumAleatorios[2]);
            checkBox4.Checked = Convert.ToBoolean(NumAleatorios[3]);
            checkBox5.Checked = Convert.ToBoolean(NumAleatorios[4]);
            checkBox6.Checked = Convert.ToBoolean(NumAleatorios[5]);
            checkBox7.Checked = Convert.ToBoolean(NumAleatorios[6]);
        }
    }
}
