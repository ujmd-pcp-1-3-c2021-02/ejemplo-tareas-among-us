﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea1 : UserControl
    {
        public ucTarea1()
        {
            InitializeComponent();
        }

        private void ucTarea1_Load(object sender, EventArgs e)
        {
            panelGasolina.Height = 0;
        }

        private void btnLlenar_MouseDown(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = true;
            btnLedRojo.BackColor = Color.Red;
        }

        private void btnLlenar_MouseUp(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = false;
            btnLedRojo.BackColor = Color.FromArgb(64, 0, 0);
        }

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (panelGasolina.Height < PanelTanque.Height)
            {
                panelGasolina.Height += 4;
            }
            else
            {
                btnLedVerde.BackColor = Color.Lime;
                btnLedRojo.BackColor = Color.FromArgb(64, 0, 0);
                btnLlenar.Enabled = false;
                tmrLlenar.Enabled = false;
                lblTítulo.ForeColor = Color.Lime;

                frmPrincipal.TareasCompletadas[0] = true;
            }
        }
    }
}
