﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea5 : UserControl
    {
        public ucTarea5()
        {
            InitializeComponent();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Width == 50)
            {
                pictureBox1.Width = 44;
                pictureBox1.Left += 3;
            }
            else if (pictureBox1.Width == 44)
            {
                pictureBox1.Width = 38;
                pictureBox1.Left += 3;
            }
            else if (pictureBox1.Width == 38)
            {
                pictureBox1.Width = 32;
                pictureBox1.Left += 3;
            }
            else
            {
                pictureBox1.Visible = false;
                if (!pictureBox1.Visible && !pictureBox2.Visible &&
                    !pictureBox3.Visible && !pictureBox4.Visible)
                {
                    lblStatus.Text = "OK";
                    lblStatus.ForeColor = Color.Lime;
                    label1.Text = "TAREA 5 COMPLETADA";
                    label1.ForeColor = Color.Lime;

                    frmPrincipal.TareasCompletadas[4] = true;
                }
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox2.Width == 50)
            {
                pictureBox2.Width = 44;
                pictureBox2.Left += 3;
            }
            else if (pictureBox2.Width == 44)
            {
                pictureBox2.Width = 38;
                pictureBox2.Left += 3;
            }
            else if (pictureBox2.Width == 38)
            {
                pictureBox2.Width = 32;
                pictureBox2.Left += 3;
            }
            else
            {
                pictureBox2.Visible = false;
                if (!pictureBox1.Visible && !pictureBox2.Visible &&
                    !pictureBox3.Visible && !pictureBox4.Visible)
                {
                    lblStatus.Text = "OK";
                    lblStatus.ForeColor = Color.Lime;
                    label1.Text = "TAREA 5 COMPLETADA";
                    label1.ForeColor = Color.Lime;

                    frmPrincipal.TareasCompletadas[4] = true;
                }
            }
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox3.Width == 50)
            {
                pictureBox3.Width = 44;
                pictureBox3.Left += 3;
            }
            else if (pictureBox3.Width == 44)
            {
                pictureBox3.Width = 38;
                pictureBox3.Left += 3;
            }
            else if (pictureBox3.Width == 38)
            {
                pictureBox3.Width = 32;
                pictureBox3.Left += 3;
            }
            else
            {
                pictureBox3.Visible = false;
                if (!pictureBox1.Visible && !pictureBox2.Visible &&
                    !pictureBox3.Visible && !pictureBox4.Visible)
                {
                    lblStatus.Text = "OK";
                    lblStatus.ForeColor = Color.Lime;
                    label1.Text = "TAREA 5 COMPLETADA";
                    label1.ForeColor = Color.Lime;

                    frmPrincipal.TareasCompletadas[4] = true;
                }
            }
        }

        private void pictureBox4_MouseUp(object sender, MouseEventArgs e)
        {
            if (pictureBox4.Width == 50)
            {
                pictureBox4.Width = 44;
                pictureBox4.Left += 3;
            }
            else if (pictureBox4.Width == 44)
            {
                pictureBox4.Width = 38;
                pictureBox4.Left += 3;
            }
            else if (pictureBox4.Width == 38)
            {
                pictureBox4.Width = 32;
                pictureBox4.Left += 3;
            }
            else
            {
                pictureBox4.Visible = false;
                if (!pictureBox1.Visible && !pictureBox2.Visible &&
                    !pictureBox3.Visible && !pictureBox4.Visible)
                {
                    lblStatus.Text = "OK";
                    lblStatus.ForeColor = Color.Lime;
                    label1.Text = "TAREA 5 COMPLETADA";
                    label1.ForeColor = Color.Lime;

                    frmPrincipal.TareasCompletadas[4] = true;
                }
            }
        }
    }
}
