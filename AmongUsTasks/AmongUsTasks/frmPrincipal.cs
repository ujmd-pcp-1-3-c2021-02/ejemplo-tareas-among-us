﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class frmPrincipal : Form
    {
        public static bool[] TareasCompletadas = new bool[6];

        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void rbtnHome_CheckedChanged(object sender, EventArgs e)
        {
            panelHome.Visible = rbtnHome.Checked;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea11.Visible = radioButton1.Checked;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea21.Visible = radioButton2.Checked;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea31.Visible = radioButton3.Checked;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea41.Visible = radioButton4.Checked;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea51.Visible = radioButton5.Checked;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea61.Visible = radioButton6.Checked;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (TareasCompletadas[0])
            {
                radioButton1.BackColor = Color.Lime;
                radioButton1.Enabled = false;
            }
            if (TareasCompletadas[1])
            {
                radioButton2.BackColor = Color.Lime;
                radioButton2.Enabled = false;
            }
            if (TareasCompletadas[2])
            {
                radioButton3.BackColor = Color.Lime;
                radioButton3.Enabled = false;
            }
            if (TareasCompletadas[3])
            {
                radioButton4.BackColor = Color.Lime;
                radioButton4.Enabled = false;
            }
            if (TareasCompletadas[4])
            {
                radioButton5.BackColor = Color.Lime;
                radioButton5.Enabled = false;
            }
            if (TareasCompletadas[5])
            {
                radioButton6.BackColor = Color.Lime;
                radioButton6.Enabled = false;
            }

            if (TareasCompletadas[0] && TareasCompletadas[1] && 
                TareasCompletadas[2] && TareasCompletadas[3] && 
                TareasCompletadas[4] && TareasCompletadas[5])
            {
                timer1.Enabled = false;
                MessageBox.Show("Tareas finalizadas exitosamente.");
            }
        }
    }
}
