﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea2 : UserControl
    {
        public ucTarea2()
        {
            InitializeComponent();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            pbxDescarga.Image = Properties.Resources.among_us_download_gif;
            btnDownload.Visible = false;
            progressBar1.Visible = true;
            lblPorcentaje.Visible = true;
            lblTiempoEstimado.Visible = true;
            tmrDescargar.Enabled = true;
        }

        private void tmrDescargar_Tick(object sender, EventArgs e)
        {
            switch (progressBar1.Value)
            {
                case 0:
                    progressBar1.Value += 14;
                    lblTiempoEstimado.Text = "Estimated Time: 11hr 25m";
                    break;
                case 14:
                    progressBar1.Value += 14;
                    lblTiempoEstimado.Text = "Estimated Time: 3hr 53m";
                    break;
                case 28:
                    progressBar1.Value += 14;
                    lblTiempoEstimado.Text = "Estimated Time: 2hr 6m 2s";
                    break;
                case 42:
                    progressBar1.Value += 14;
                    lblTiempoEstimado.Text = "Estimated Time: 34m 48s";
                    tmrDescargar.Interval = 50;
                    break;
                case 100:
                    pbxDescarga.Image = Properties.Resources.DescargaArchivos;
                    lblTiempoEstimado.Text = "Completed";
                    tmrDescargar.Enabled = false;
                    lblTítulo.ForeColor = Color.Lime;

                    frmPrincipal.TareasCompletadas[1] = true;
                    break;
                default:
                    int MilisegundosRestantes = (100 - progressBar1.Value) * tmrDescargar.Interval;
                    progressBar1.Value++;
                    lblTiempoEstimado.Text = "Estimated Time: " + (1 + MilisegundosRestantes/1000) + "s";
                    break;
            }
            lblPorcentaje.Text = progressBar1.Value + "%";
        }
    }
}
