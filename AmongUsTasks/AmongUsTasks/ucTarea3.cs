﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmongUsTasks
{
    public partial class ucTarea3 : UserControl
    {
        int NumeroActual = 0;
        public ucTarea3()
        {
            InitializeComponent();
        }

        private void ucTarea3_Load(object sender, EventArgs e)
        {
            int[] Numeros = new int[10];

            var rnd = new Random();

            // El loop While se encarga de generar un número aleatrio para cada
            // elemento de la matriz  Numeros[] que tiene 10 elementos
            int i = 0;
            while (i < Numeros.Length)
            {
                int NumAleatorio = rnd.Next(1, 11);

                bool Existente = false;

                // El loop tipo For se encarga de revisar si el número aleatorio generado
                // en este momento existía en los elementos anteriores o no
                for (int j = 0; j < i; j++)
                {
                    if (NumAleatorio == Numeros[j])
                    {
                        Existente = true;
                        break;
                    }
                }

                if (Existente == false)
                {
                    Numeros[i] = NumAleatorio;
                    i++;
                }
            }

            checkBox1.Text = Numeros[0].ToString();
            checkBox2.Text = Numeros[1].ToString();
            checkBox3.Text = Numeros[2].ToString();
            checkBox4.Text = Numeros[3].ToString();
            checkBox5.Text = Numeros[4].ToString();
            checkBox6.Text = Numeros[5].ToString();
            checkBox7.Text = Numeros[6].ToString();
            checkBox8.Text = Numeros[7].ToString();
            checkBox9.Text = Numeros[8].ToString();
            checkBox10.Text = Numeros[9].ToString();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            ucTarea3_Load(sender, e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkPresionado = (CheckBox) sender;

            if (chkPresionado.Checked == true)
            {
                int NumeroPresionado = Convert.ToInt32(chkPresionado.Text);

                if (NumeroPresionado == NumeroActual + 1)
                {
                    // Correcto
                    NumeroActual = NumeroPresionado;
                    if (NumeroActual == 10)
                    {
                        //Tarea completada
                        label1.Text = "Tarea 3 completada";
                        label1.ForeColor = Color.Lime;
                        checkBox1.Enabled = false;
                        checkBox2.Enabled = false;
                        checkBox3.Enabled = false;
                        checkBox4.Enabled = false;
                        checkBox5.Enabled = false;
                        checkBox6.Enabled = false;
                        checkBox7.Enabled = false;
                        checkBox8.Enabled = false;
                        checkBox9.Enabled = false;
                        checkBox10.Enabled = false;

                        frmPrincipal.TareasCompletadas[2] = true;
                    }
                }
                else
                {
                    // Incorrecto
                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;
                    checkBox8.Checked = false;
                    checkBox9.Checked = false;
                    checkBox10.Checked = false;
                    NumeroActual = 0;
                }
            }
            
        }
    }
}
